from lib.sql.models import Base
from sqlalchemy import Column, Integer, String

class Image(Base):
    __tablename__ = "images"

    id = Column(Integer, sqlite_autoincrement=True, primary_key=True, nullable=False)
    location = Column(String, nullable=False)
    remote = Column(Integer, nullable=False)