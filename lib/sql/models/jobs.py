from lib.sql.models import Base
from sqlalchemy import Column, String, Integer

class Job(Base):
    __tablename__ = "jobs"

    id = Column(String(50), nullable=False, primary_key=True)
    room = Column(String, nullable=False)