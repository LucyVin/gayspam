from lib.sql.models import Base
from sqlalchemy import Column, Integer, String

class User(Base):
    __tablename__ = "users"

    id = Column(String, primary_key=True)
    name = Column(String, nullable=False, unique=True)
    access = Column(String, default=1, nullable=False) # JSON payload {`room`:`access level`, or *}