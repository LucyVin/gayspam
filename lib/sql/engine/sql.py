from sqlalchemy import create_engine
from sqlalchemy_utils import database_exists, create_database
from sqlalchemy.orm import sessionmaker
import logging

class Backend:
    def __init__(self, conn_string="sqlite://"):
        if conn_string == "sqlite://":
            msg = "No sqlite database file provided, using in-memory database. "
            msg = msg + "To store information beyond a single session, please set this value."
            logging.warning(msg)

        self.conn_string = conn_string
        self.engine = create_engine(conn_string)

        if not database_exists(conn_string):
            create_database(conn_string)

        self.Session = sessionmaker(bind=self.engine)
