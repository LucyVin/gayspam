from lib.sql.models import Base
from sqlalchemy import Column, Integer, String

class Room(Base):
    __tablename__ = "rooms"

    id = Column(Integer, sqlite_autoincrement=True, nullable=False, primary_key=True)
    name = Column(String, nullable=False)