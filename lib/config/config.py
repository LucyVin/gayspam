import json
import logging
import validators
from os import path
from lib.config import ConfigException

DEFAULT_CONFIG="~/.local/share/gayspam/default.json"


class Config:
    def __init__(
        self, 
        username=None, 
        homeserver=None, 
        password=None,
        auto_rooms=[],
        backend = 'sqlite',
        storage = 'remote',
        storage_dir = None,
        timer_delay = "5"
        ):
        """
        username: 
        matrix username
        homeserver:
        valid homeserver url for matrix connection
        password:
        plaintext password for matrix user
        auto_rooms:
        list of rooms to join on start/restart
        backend:
        sql storage backend for the service (currently only supports sqlite)
        storage:
        sets remote or local image storage. 
        If remote, will point to URLs of images to use, if local, will point to 
        files on disk. If mixed, will use both URL and local file storage options
        (this may cause high load)
        storage_dir:
        if using local files, the directory which contains those files
        """

        # we'll load these via JSON or CLI
        self.username = username
        self.homeserver = homeserver
        self.password = password
        self.auto_rooms = auto_rooms
        self.backend = backend
        self.storage = storage
        self.storage_dir = storage_dir
        self.timer_delay = timer_delay

    def _auto_get_config(self):
        fp = path.expanduser(DEFAULT_CONFIG)
        conf_file = open(fp, 'rb').read()
        conf = json.loads(conf_file)
        
        self.username = conf.get('username')
        self.homeserver = conf.get('homeserver')
        self.password = conf.get('password')
        self.auto_rooms = conf.get('auto_rooms')
        self.backend = conf.get('backend')
        self.storage = conf.get('storage')
        self.stoage_dir = conf.get('storage_dir')
        self.timer_delay = conf.get('timer_delay')

    def _validate_homeserver_url(self, homeserver=None):
        homeserver = homeserver or self.homeserver

        check = validators.url(homeserver)
        if not check:
            raise ConfigException("{} Is not a valid homeserver url")
        
        return check

    def _build_user_string(self, username=None, homeserver=None):
        username = username or self.username
        homeserver = homeserver or self.homeserver

        if not username and not homeserver:
            raise ConfigException("Username or homeserver not found in config or CLI arguments")

        return f"@{username}:{homeserver}"

    def _delay_to_seconds(self, timer_delay):
        if timer_delay[-1] not in ("s", "m") and not int(timer_delay):
            logging.warning("timer delay value {} not valid int or '%d[s|m]' format, using default")            
            self.timer_delay = "5"

        elif int(timer_delay):
            # ensure string
            self.timer_delay = str(timer_delay)

        else:
            # coerce to seconds
            self.timer_delay = str(int(timer_delay[:-1])*60)