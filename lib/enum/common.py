from enum import Enum

class UserEnum(Enum):
    BASIC = 1
    MODERATOR = 2
    ADMIN = 3