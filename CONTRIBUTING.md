# To Contribute: 
- Setup dev environment using virtualenv (python 3.8+)
- commit changes to branch on fork
- submit PR with detailed changes and tests
- await approval 
