from rq.job import Job
from rq.serializers import send_stop_job_command
from job_worker.client import Client
import logging

class Worker:
    def __init__(self, client, idx=None, function=None):
        self.idx = idx
        self.function = function
        self.client = client

    def _create(self, idx=None, args=None, function=None):
        if not function and not self.function:
            raise Exception("No function supplied")

        self.job = Job.create(function, args, id=idx)
        return job

    def _run(self, job=self.job):
        if not job:
            raise Exception("No job provided")

        return self.client.queue.enqueue_job(job)

    def create(self, function, args, idx=None):
        f = function

        job = Job.create(
            function=f,
            id=idx,
            args=args
        )

        return self._run(job)

    def halt(self, job=None, idx=None):
        # should only be called in an emergency situation
        if not job and not idx:
            raise Exception("No job or job ID provided")

        if job:
            idx = job.id

        logging.critical("Sending stop job command to redis for {}".format(idx))
        send_stop_job_command(self.client.redis, idx)
        
