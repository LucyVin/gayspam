FROM ubuntu:focal
MAINTAINER Lucy lucy@lucyvin.com

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get install -y build-essential
RUN apt-get install -y libolm-dev
RUN git clone https://gitlab.com/lucyvin/gayspam
RUN pip install -r gayspam/requirements.txt
RUN wget http://download.redis.io/redis-stable.tar.gz
RUN tar -xvzf redis-stable.tar.gz
RUN cd redis-stable && make